#ifndef		__INT_2_SPANISH_H__
#define		__INT_2_SPANISH_H__

#include <string>

std::string int2spanish(int n);

#endif	// __INT_2_SPANISH_H__